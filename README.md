# auxcms

#### 介绍
AuxCMS框架是一款PHP8高性能·简单易用内容管理系统 支持的微信公众号，小程序，APP客户端，移动端网站，PC网站等。
基于xunruicms二次开发的内容管理系统、 适合企业或个人快速建站、 模板建站、对SEO友好、 由PHP+MySQL+Codeigniter架构，基于MIT开源协议发布，免费且不限制商业使用，允许开发者自由修改前后台界面中的版权信息。 

#### 特色功能


#### 运行环境
PHP7.4+（支持PHP8）， MySQL5以上，推荐5.7及以上 Apache、Nginx、IIS等Web服务器都可以

#### 安装教程

将代码下载到网站的根目录，切忌不要放在子目录安装
运行环境检查程序 /test.php（安装完后删除更安全）
环境通过后，运行安装文件 /install.php（安装完后删除更安全）
默认后台入口文件是 /admin.php（自行更改后台入口文件名称更安全）

#### 标签
Codeigniter
ThinkPHP
xunruicms
AuxCMS